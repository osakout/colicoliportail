import Vue from 'vue';
import Router from 'vue-router';
import Acceuil2 from '../pages/Acceuil2';
import NosValeurs from '../pages/NosValeurs';
import LegalNotice from '../pages/LegalNotice';
import DevenirPartenaires from '../pages/DevenirPartenaires';
import Recrutement from '../pages/Recrutement';
import DevenirPartenairesForm from '../pages/DevenirPartenairesForm';
import DevenirClientForm from '../pages/DevenirClientForm';
import Ecommercant from '../pages/E-commercants';
import Boutiques from '../pages/Boutiques';
import Transporteurs from '../pages/Transporteurs';
import SuivisColis from '../pages/SuivisColis';
import QuiSommesNous from '../pages/Quisommesnous';

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            redirect: '/accueil',
            props: true
        },
        {
            path: '/notice',
            name: 'LegalNotice',
            component: LegalNotice,
            props: true
        },
        {
            path: '/accueil',
            name: 'Acceuil2',
            component: Acceuil2,
            props: true
        },
        {
            path: '/nosValeurs',
            name: 'NosValeurs',
            component: NosValeurs,
            props: true
        },
        {
            path: '/devenirPartenaires',
            name: 'DevenirPartenaires',
            component: DevenirPartenaires,
            props: true
        },
        {
            path: '/recrutement',
            name: 'Recrutement',
            component: Recrutement,
            props: true
        },
        {
            path: '/devenirPartenaires/formulaire',
            name: 'DevenirPartenairesForm',
            component: DevenirPartenairesForm,
            props: true
        },
        {
            path: '/e-commercants',
            name: 'E-commercant',
            component: Ecommercant,
            props: true
        },
        {
            path: '/boutiques',
            name: 'Boutiques',
            component: Boutiques,
            props: true
        },
        {
            path: '/transporteurs',
            name: 'Transporteurs',
            component: Transporteurs,
            props: true
        },
        {
            path: '/devenirClient',
            name: 'DevenirClientForm',
            component: DevenirClientForm,
            props: true
        },
        {
            path: '/suivisColis',
            name: 'SuivisColis',
            component: SuivisColis,
            props: true
        },
        {
            path: '/quiSommesNous',
            name: 'QuiSommesNous',
            component: QuiSommesNous,
            props: true
        }
    ]
})